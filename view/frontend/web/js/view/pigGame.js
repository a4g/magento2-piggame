define(['ko'], function(ko){
    'use strict';
    return function(config){
        const winn_border = 100;
        let currentPlayer=0;
        const viewModel = {
            btnTurn: ko.observable(false),
            players: config.players,
            dice1: ko.observable(1),
            dice2: ko.observable(1),
            game: ko.observableArray([
                {
                    round: 1,
                    scores: ['-','-'],
                }
            ])
        };
        /**
         * Returns totals
         */
        viewModel.totals = ko.computed(
            function () {
                const game  = this.game();
                // TODO: Refactor to each
                return[
                    game.reduce((a, b) => a + ('-' === b.scores[0] ? 0 : b.scores[0]), 0),
                    game.reduce((a, b) => a + ('-' === b.scores[1] ? 0 : b.scores[1]), 0)
                ]
            }.bind(viewModel)
        );
        /**
         * Rolling the dice
         */
        viewModel.roll = function () {
            this.btnTurn(true);
            this.dice1(GameController.rollSingleDice());
            this.dice2(GameController.rollSingleDice());
            if (0 === GameController.getResult(this.dice1(),this.dice2())){
                this.turn();
            }
        };

        /**
         * Next game turn
         */
        viewModel.turn = function () {
            this.btnTurn(false);
            const round = this.game().length;
            let scores  = this.game()[round-1].scores;
            const result = GameController.getResult(this.dice1(), this.dice2());
            scores[currentPlayer] = result<0 ? -1 * this.totals[currentPlayer] : result; //snake eyes
            this.game.splice(round-1,1,{'round':round, 'scores': scores});
            if (this.totals()[0] >= winn_border || this.totals()[1] >=winn_border){
                alert(this.players[currentPlayer] + ' is winning');
                // TODO RESET GAME
                return;
            }
            GameController.switchPlayer();
            if (0 === currentPlayer % config.players.length) {
                this.game.push(                {
                    round: round +1,
                    scores: ['-','-']
                })
            }
        };

        const GameController ={
            rollSingleDice: function () {
                return 1 + Math.floor(Math.random()*6);
            },
            switchPlayer: function () {
                currentPlayer  = (currentPlayer + 1) % config.players.length;
            },
            /**
             * Counts final turn score
             * @param {int} a dice 1 points
             * @param {int} b dice 2 points
             * @returns {number|*}
             */
            getResult: function(a,b){
                var diceSum = a + b;
                if (2 === diceSum) { //snake eyes
                    return -1;
                }
                if (1 === a || 1 === b){
                    return 0;
                }
                return diceSum;
            },
        };


        return viewModel
    }
});

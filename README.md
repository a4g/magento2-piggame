# Magento 2 Dice Pig Game Module

 - [Instalation] 
 - [Instalation as a git submodule] 
 - [Play]

## Installation
 - download/copy files to [magento]/app/code/MichalSagan/PigGame
 - ```bin/magento setup:update```

## Instalation as a git submodule
 - ```cd [magento2_dir]```
 - ``git submodule init git@gitlab.com:a4g/erp-integrations-base.git app/code/MichalSagan/PigGame`` 
 - ```bin/magento setup:update```

## Play visit:
[yourmagentoUrl]/piggame